import { Component, OnInit } from '@angular/core';

@Component({
  selector: '<app-root>',
  template: `
    <app-customers> </app-customers>
  `
})

export class AppComponent implements OnInit{
  title :string;
  constructor(){}
  ngOnInit(){
    // initialise the data here 
    this.title = "Hello world using data binding !";
  }
}
