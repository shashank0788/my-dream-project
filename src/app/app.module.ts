import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'; // directives You don't import Browser Module more than once; and specially not in child components

import { CustomersModule } from './customers/customers.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [ AppComponent ], // what is inside this module
  imports: [ BrowserModule, CustomersModule, SharedModule ],
  bootstrap: [AppComponent] // which one is the first one to get printed on UI
})
export class AppModule {}