import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'; // directives

import { CustomersComponent } from './customers.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { FilterTextboxComponent } from './customers-list/filter-textbox/filter-textbox.component';
import {SharedModule} from '../shared/shared.module';
@NgModule({
  declarations: [ CustomersComponent, CustomersListComponent, FilterTextboxComponent ], // what is inside this module
  imports: [ CommonModule, SharedModule, FormsModule ],
  exports: [ CustomersComponent ] // Temporary thing  makes sure who imports this module gets access to componenets
})
export class CustomersModule {}