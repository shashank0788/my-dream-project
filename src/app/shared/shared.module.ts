import { NgModule } from '@angular/core';

import { CapitalizePipe} from './capitalize.pipe';
// ANy thing that can be used in multile Places ... .Goes here 
@NgModule({
    declarations: [ CapitalizePipe ],
    exports: [ CapitalizePipe ]
})
export class SharedModule{}